## Description
Ce server fait partie d'un POC pour tester la transmission de données entre un service Node.js Express et un service Java avec Protocol Buffers.

Il reçoit une date et une heure, les utilise pour récupérer des informations sur les marées, crée un objet qu'il sérialise et transmet via une requête http à un service en Java.
Le service Java renvoie en retour l'objet sérialisé contenant l'ensemble des informations ainsi que la hauteur d'eau au port de Saint-Malo calculée à l'instant demandé.


## Protobuf
Les messages échangés doivent être définis dans un fichier ".proto", qui constitue le schéma de validation des objects.

Ce fichier doit ensuite être compilé par le compilateur "protoc", précédemment installé sur l'ordinateur (https://github.com/protocolbuffers/protobuf/releases/tag/v21.12), avec la ligne de commande suivante :

``protoc --js_out=import_style=commonjs,binary:. tides.proto``

Node requiert également l'installation de la dépendance 'google-protobuf' : https://www.npmjs.com/package/google-protobuf

Protobuf overview : https://developers.google.com/protocol-buffers/docs/overview

## Tests
Ce server expose un point d'API GraphQL à l'adresse suivante : http://localhost:4000/graphql

Pour lancer le server : ``node index.js``

Pour le tester, on entre une date (entre le 23 et le 25 janvier) et une heure pour connaitre la hauteur de la marée à ce moment à Saint-Malo.

Requête a saisir dans le playground : 

query {
whatHeight(request : {date: "2023-01-24", time: "14:35:00"})
}
