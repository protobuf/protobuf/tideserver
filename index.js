﻿const express = require('express');
const axios = require('axios');
const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');

//Import protobuf tide object schema
const protobufSchema = require("./tides_pb");
const {exec} = require("child_process");

// TYPEDEFS : Construct a schema, using GraphQL schema language
const schema = buildSchema(`
  input RequestInput {
    date: String
    time: String
  }
  type Query {
    whatHeight(request : RequestInput!) : String
    whatHeightJson(request : RequestInput!) : String
  }
`);

//RESOLVERS : The root provides a resolver function for each API endpoint
const root = {
    whatHeight: (requestParams) => {
        return getHeight(requestParams.request);
    },
    whatHeightJson: (requestParams) => {
        return getHeightJson(requestParams.request);
    }
};

//DATA : Some data about tides on the 23th, 24th and 25th of january
const tidesData = [
    {
        dateTime: convertToDateTime("2023-01-23", "02:47:00"),
        tide: "1.36"
    },
    {
        dateTime: convertToDateTime("2023-01-23", "08:11:00"),
        tide: "12.69"
    },
    {
        dateTime: convertToDateTime("2023-01-23", "15:16:00"),
        tide: "0.86"
    },
    {
        dateTime: convertToDateTime("2023-01-23","20:40:00"),
        tide: "12.47"
    },
    {
        dateTime: convertToDateTime("2023-01-24", "03:37:00"),
        tide: "1.14"
    },
    {
        dateTime: convertToDateTime("2023-01-24", "08:58:00"),
        tide: "12.86"
    },
    {
        dateTime: convertToDateTime("2023-01-24", "16:04:00"),
        tide: "0.75"
    },
    {
        dateTime: convertToDateTime("2023-01-24", "21:25:00"),
        tide: "12.45"
    },
    {
        dateTime: convertToDateTime("2023-01-25", "04:22:00"),
        tide: "1.20"
    },
    {
        dateTime: convertToDateTime("2023-01-25", "09:42:00"),
        tide: "12.70"
    },
    {
        dateTime: convertToDateTime("2023-01-25", "16:48:00"),
        tide: "0.98"
    },
    {
        dateTime: convertToDateTime("2023-01-25", "22:07:00"),
        tide: "12.12"
    }
]

//DATASOURCES : functions and logic
async function getHeight(requestParams) {

    try {

        //Convert requested time into ISO string
        let time = convertToDateTime(requestParams.date, requestParams.time);

        //Get data for selected day
        console.time('Search database : ');
        const index = tidesData.findIndex((el, ix) => (
            el.dateTime === time && ix-1 > -1) ||  el.dateTime > time
        );
        if (index <= 0) return "no data for this date";

        const previousTide = tidesData[index-1];
        const nextTide = tidesData[index];
        console.timeEnd('Search database : ');

        //create object Tide with proto class
        const tide = new protobufSchema.Tide();

        //Set tide parameters
        tide.setRequestedDateTime(time);
        tide.setPreviousTideDateTime(previousTide.dateTime);
        tide.setPreviousTideHeight(parseFloat(previousTide.tide));
        tide.setNextTideDateTime(nextTide.dateTime);
        tide.setNextTideHeight(parseFloat(nextTide.tide));

        //serialize object with protobuf
        const serializedTide = tide.serializeBinary();

        //Send serialized object to computing service
        const res = await sendToComputingService(serializedTide);

        //Create objet tideHeight and populate with deserialized data
        const tideHeight = protobufSchema.TideHeight.deserializeBinary(Array.from(res));

        //return height of high tide (to change after call to service has been made
        return "At " + requestParams.time + " on the " + requestParams.date + " the sea level will be at " + tideHeight.getTideHeight().toFixed(2) + " meters";

    } catch (e){
        console.log("error : " + e);
    }
}


async function getHeightJson(requestParams) {

    try {

        //Convert requested time into ISO string
        let time = convertToDateTime(requestParams.date, requestParams.time);

        //Get data for selected day
        const index = tidesData.findIndex((el, ix) => (el.dateTime === time && ix-1 > -1) ||  el.dateTime > time);
        if (index <= 0) return "no data for this date";

        const previousTide = tidesData[index-1];
        const nextTide = tidesData[index];

        //create object Tide with Json
        const tideJson = {
            "requestedDateTime" : time,
            "previousTideDateTime" : previousTide.dateTime,
            "previousTideHeight" : previousTide.tide,
            "nextTideDateTime" : nextTide.dateTime,
            "nextTideHeight" : nextTide.tide,
        };

        //Send serialized object to computing service
        const resJson = await sendToComputingServiceJson(tideJson);

        //return height of high tide (to change after call to service has been made
        return "At " + requestParams.time + " on the " + requestParams.date + " the sea level will be at " + resJson.toFixed(2) + " meters";

    } catch (e){
        console.log("error : " + e);
    }
}

//Convert received date and time into ISO date
function convertToDateTime(dateString, timeString) {
    return new Date(dateString + "T" + timeString).toISOString();
}

//Sending to computing service
async function sendToComputingService(data) {
    try {
        return await axios.post("http://localhost:4567/tide", data, {
            headers: {
                "Content-Type":"application/protobuf",
                "Accept":"application/protobuf"
            },
            responseType: 'arraybuffer'
        }).then(res => res.data)
    } catch (err) {
        console.log(err);
    }
}

//Sending to computing service with JSON
async function sendToComputingServiceJson(data) {
    try {
        return await axios.post("http://localhost:4567/tideJson", data, {
            headers: {
                "Content-Type":"application/json",
                    "Accept":"application/json"
            }})
            .then(res => res.data)
    } catch (err) {
        console.log(err);
    }
}

//MAIN

//execute compilation of proto file
exec("protoc --js_out=import_style=commonjs,binary:. tides.proto", (error) => {
    if (error) {
        console.log(`error: ${error.message}`);
    }
});

//Run server
const app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');